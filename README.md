## Gitlab Runner K8S


### Pre instalação
Verificar apontamento do cluster.

export KUBECONFIG=kube-config/devops.yaml
kubectl get nodes

Criar os segredos necessarios para a instalação
kubectl apply -f gitlab-runner.secret.yaml -n gitlab-runner-gelin
kubectl get secrets -n gitlab-runner-gelin

### Instalação do Gitlab Runner
Adicionar o repositorio Helm.
helm repo add gitlab https://charts.gitlab.io
helm repo update

Instalar o Gitlab Runner no cluster k8s.
helm install --namespace gitlab-runner-gelin gitlab-runner-gelin -f values.yaml gitlab/gitlab-runner

Upgrade o Gitlab Runner no cluster k8s. (só se modificar algo)
helm upgrade --namespace gitlab-runner-gelin gitlab-runner-gelin -f values.yaml gitlab/gitlab-runner

# Desinstalar
```bash
helm uninstall gitlab-runner-gelin -n gitlab-runner-gelin
```